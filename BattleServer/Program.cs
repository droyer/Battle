﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using System.Threading;
using System.IO;

namespace BattleServer
{
    public class Program
    {
        public static Server server;
        public static Map map;

        static GameUpdate gameUpdate;
        static Thread inputThread;

        static int port;

        static void Main(string[] args)
        {
            server = new Server();
            if (!ReadIni() || !server.Start(port))
            {
                Console.WriteLine("Server couln't start");
                Console.Read();
                return;
            }
            map = new Map(server);

            gameUpdate = new GameUpdate();
            gameUpdate.OnUpdate += Update;

            inputThread = new Thread(InputThread);
            inputThread.Start();
            inputThread.Priority = ThreadPriority.Lowest;

            while (true)
            {
                gameUpdate.Tick();
            }
        }

        private static void Update(GameTime gameTime)
        {
            if (map != null)
                map.Update(gameTime);
        }

        static void InputThread()
        {
            while (true)
            {
                string cmd = Console.ReadLine();

                if (cmd == "time day")
                    map.daying = true;
                else if (cmd == "time night")
                    map.daying = false;
            }
        }

        static bool ReadIni()
        {
            string file = "server.ini";
            if (!File.Exists(file))
                return false;

            using (StreamReader sr = new StreamReader(file))
            {
                try
                {
                    string line = sr.ReadLine();
                    string[] args = line.Split('=');
                    if (args[0] == "port")
                        port = Convert.ToInt32(args[1]);

                    sr.Close();
                }
                catch { return false; }
            }
            return true;
        }
    }
}
