﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lidgren.Network;
using Lidgren.Network.Xna;
using Microsoft.Xna.Framework;

namespace BattleServer
{
    public enum BlockID
    {
        None = 0,
        Dirt = 1,
        Stone = 2,
        Torch = 3
    }

    public class Map
    {
        public static int SIZE = 500;
        public static bool READY = false;

        Random random;
        Block[,] blocks;
        List<Player> playerList;
        Server server;

        Vector2 spawnPosition;
        Rectangle spawnRectangle;
        byte idOffset = 0;

        float lightAlpha = 0;
        public bool daying = true;

        public Map(Server server)
        {
            this.server = server;
            random = new Random();

            Generate();
        }

        void Generate()
        {
            Console.WriteLine("Generating world...");
            blocks = new Block[SIZE, SIZE];

            for (int y = 0; y < SIZE; y++)
            {
                for (int x = 0; x < SIZE; x++)
                {
                    if (x == 0 || y == 0 || x == SIZE - 1 || y == SIZE - 1)
                        blocks[x, y] = new BlockStone(x, y);
                    else
                    {
                        if (random.Next(0, 10) == 0)
                        {
                            blocks[x, y] = new BlockStone(x, y);
                            blocks[x, y].oldID = BlockID.Dirt;
                        }
                        else
                            blocks[x, y] = new BlockDirt(x, y);
                    }
                }
            }

            do
            {
                int x = random.Next(0, SIZE);
                int y = random.Next(0, SIZE);
                Block b = blocks[x, y];
                if (b.ID == BlockID.Dirt)
                {
                    spawnPosition = b.Position;
                    spawnRectangle = new Rectangle((int)spawnPosition.X - 8, (int)spawnPosition.Y - 8, 16, 16);
                    break;
                }
            } while (true);

            Console.WriteLine("World ready");
            READY = true;

            playerList = new List<Player>();
        }

        public void SendWorldInfo(NetIncomingMessage inc)
        {
            NetOutgoingMessage outmsg = server.CreateMessage();
            outmsg.Write((byte)Data.WorldInfo);
            outmsg.Write(SIZE);
            server.SendMessage(outmsg, inc.SenderConnection, NetDeliveryMethod.ReliableOrdered);
        }

        public void SendWorld(NetIncomingMessage inc)
        {
            NetOutgoingMessage outmsg = server.CreateMessage();
            Console.WriteLine("World sending...");
            outmsg.Write((byte)Data.World);
            int i = 0;
            foreach (Block b in blocks)
            {
                outmsg.Write((byte)b.ID);
                outmsg.Write((byte)b.oldID);

                if ((i + 1) % 100 == 0)
                {
                    server.SendMessage(outmsg, inc.SenderConnection, NetDeliveryMethod.ReliableOrdered);
                    outmsg = server.CreateMessage();
                    outmsg.Write((byte)Data.World);
                }
                i++;
            }

            Console.WriteLine("World sended");
        }

        public void NewPlayer(NetIncomingMessage inc)
        {
            NetConnection connection = inc.SenderConnection;
            Player player = new Player(connection, idOffset);
            idOffset++;
            playerList.Add(player);
            player.SetPosition(spawnPosition);

            NetOutgoingMessage outmsg = server.CreateMessage();
            outmsg.Write((byte)Data.PlayerList);
            outmsg.Write((byte)playerList.Count);
            foreach (Player p in playerList)
            {
                outmsg.Write(p.ID);
                outmsg.Write(p.Position);
            }
            server.SendToAll(outmsg, NetDeliveryMethod.ReliableOrdered, 0);
        }

        public void ReadPlayerPos(byte id, Vector2 position)
        {
            Player p = GetPlayer(id);
            if (p != null)
                p.SetPosition(position);
        }

        public Player GetPlayer(NetConnection connection)
        {
            foreach (Player p in playerList)
                if (p.Connection == connection)
                    return p;
            return null;
        }

        public Player GetPlayer(byte id)
        {
            foreach (Player p in playerList)
                if (p.ID == id)
                    return p;
            return null;
        }

        public void RemovePlayer(Player player)
        {
            playerList.Remove(player);
        }

        public void ChangeBlock(NetIncomingMessage inc)
        {
            BlockID id = (BlockID)inc.ReadByte();
            Point pos = inc.ReadPoint();
            Block currentBlock = blocks[pos.X, pos.Y];
            Block newBlock = null;

            foreach (Player p in playerList)
                if (p.Rectangle.Intersects(currentBlock.Rectangle))
                    return;
            if (spawnRectangle.Intersects(currentBlock.Rectangle))
                return;

            if (id == currentBlock.ID)
                return;

            if (id == BlockID.None)
            {
                if (currentBlock.oldID != BlockID.None)
                    newBlock = GetNewBlock(currentBlock.oldID, pos.X, pos.Y);
                else
                    newBlock = new BlockNone(pos.X, pos.Y);
            }
            else
            {
                newBlock = GetNewBlock(id, pos.X, pos.Y);
                if (newBlock.Solid && currentBlock.Solid)
                    return;
                if (!newBlock.Solid && currentBlock.Solid)
                    return;
                if (currentBlock.ID == BlockID.Torch)
                    return;
                if (currentBlock.ID != BlockID.None)
                    newBlock.oldID = currentBlock.ID;
            }

            blocks[pos.X, pos.Y] = newBlock;

            NetOutgoingMessage outmsg = server.CreateMessage();
            outmsg.Write((byte)Data.Block);
            outmsg.Write((byte)newBlock.ID);
            outmsg.Write(pos);
            outmsg.Write((byte)newBlock.oldID);
            server.SendToAll(outmsg, NetDeliveryMethod.ReliableOrdered, 0);
        }

        Block GetNewBlock(BlockID id, int x, int y)
        {
            if (id == BlockID.None)
                return new BlockNone(x, y);
            else if (id == BlockID.Dirt)
                return new BlockDirt(x, y);
            else if (id == BlockID.Stone)
                return new BlockStone(x, y);
            else if (id == BlockID.Torch)
                return new BlockTorch(x, y);
            return null;
        }

        public void Update(GameTime gameTime)
        {
            if (daying)
                lightAlpha -= (float)gameTime.ElapsedGameTime.TotalSeconds * 225 / 10;
            else
                lightAlpha += (float)gameTime.ElapsedGameTime.TotalSeconds * 225 / 10;

            if (lightAlpha <= 0)
                lightAlpha = 0;
            if (lightAlpha >= 225)
                lightAlpha = 225;

            SendTime();
        }

        void SendTime()
        {
            NetOutgoingMessage outmsg = server.CreateMessage();
            outmsg.Write((byte)Data.Time);
            outmsg.Write(lightAlpha);
            server.SendToAll(outmsg, NetDeliveryMethod.ReliableSequenced, 2);
        }
    }
}
