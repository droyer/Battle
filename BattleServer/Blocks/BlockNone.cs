﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleServer
{
    public class BlockNone : Block
    {
        public BlockNone(int x, int y) : base(x, y)
        {
            id = BlockID.None;
        }
    }
}
