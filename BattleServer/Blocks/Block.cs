﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleServer
{
    public class Block
    {
        public static int SIZE = 16;

        protected Vector2 position;
        protected BlockID id;
        protected Rectangle rectangle;
        protected bool solid = false;

        public BlockID oldID = BlockID.None;

        public Rectangle Rectangle
        {
            get { return rectangle; }
        }

        public Vector2 Position
        {
            get { return position; }
        }

        public BlockID ID
        {
            get { return id; }
        }

        public bool Solid
        {
            get { return solid; }
        }

        public Block(int x, int y)
        {
            position.X = x * SIZE;
            position.Y = y * SIZE;
            rectangle = new Rectangle((int)position.X - 8, (int)position.Y - 8, SIZE, SIZE);
        }
    }
}
