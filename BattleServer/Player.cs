﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace BattleServer
{
    public class Player
    {
        NetConnection connection;
        Vector2 position;
        byte id;
        Rectangle rectangle;

        public Rectangle Rectangle
        {
            get { return rectangle; }
        }

        public Vector2 Position
        {
            get { return position; }
        }

        public NetConnection Connection
        {
            get { return connection; }
        }

        public byte ID
        {
            get { return id; }
        }

        public Player(NetConnection connection, byte id)
        {
            this.connection = connection;
            this.id = id;
        }

        public void SetPosition(Vector2 position)
        {
            this.position = position;
            rectangle = new Rectangle((int)position.X - 4, (int)position.Y - 4, 8, 8);
        }
    }
}
