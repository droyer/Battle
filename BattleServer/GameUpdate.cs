﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BattleServer
{
    public class GameUpdate
    {
        private TimeSpan _targetElapsedTime = TimeSpan.FromTicks(166667); // 60fps
        private TimeSpan _maxElapsedTime = TimeSpan.FromMilliseconds(500);

        private TimeSpan _accumulatedElapsedTime;
        private readonly GameTime _gameTime = new GameTime();
        private Stopwatch _gameTimer;
        private long _previousTicks = 0;
        private int _updateFrameLag;

        public event System.Action<GameTime> OnUpdate;

        public GameUpdate()
        {
            _gameTimer = new Stopwatch();
            _gameTimer.Start();
        }

        public TimeSpan TargetElapsedTime
        {
            get { return _targetElapsedTime; }
        }

        internal void DoUpdate(GameTime gameTime)
        {
            if (OnUpdate != null)
                OnUpdate.Invoke(gameTime);
        }

        public void Tick()
        {
            RetryTick:

            var currentTicks = _gameTimer.Elapsed.Ticks;
            _accumulatedElapsedTime += TimeSpan.FromTicks(currentTicks - _previousTicks);
            _previousTicks = currentTicks;

            if (_accumulatedElapsedTime < TargetElapsedTime)
            {
                var sleepTime = (int)(TargetElapsedTime - _accumulatedElapsedTime).TotalMilliseconds;

                // NOTE: While sleep can be inaccurate in general it is 
                // accurate enough for frame limiting purposes if some
                // fluctuation is an acceptable result.
                System.Threading.Thread.Sleep(sleepTime);
                goto RetryTick;

            }

            // Do not allow any update to take longer than our maximum.
            if (_accumulatedElapsedTime > _maxElapsedTime)
                _accumulatedElapsedTime = _maxElapsedTime;


            _gameTime.ElapsedGameTime = TargetElapsedTime;

            var stepCount = 0;

            // Perform as many full fixed length time steps as we can.
            while (_accumulatedElapsedTime >= TargetElapsedTime)
            {
                _gameTime.TotalGameTime += TargetElapsedTime;
                _accumulatedElapsedTime -= TargetElapsedTime;
                ++stepCount;

                DoUpdate(_gameTime);
            }

            //Every update after the first accumulates lag
            _updateFrameLag += Math.Max(0, stepCount - 1);

            //If we think we are running slowly, wait until the lag clears before resetting it
            if (_gameTime.IsRunningSlowly)
            {
                if (_updateFrameLag == 0)
                    _gameTime.IsRunningSlowly = false;
            }
            else if (_updateFrameLag >= 5)
            {
                //If we lag more than 5 frames, start thinking we are running slowly
                _gameTime.IsRunningSlowly = true;
            }

            //Every time we just do one update and one draw, then we are not running slowly, so decrease the lag
            if (stepCount == 1 && _updateFrameLag > 0)
                _updateFrameLag--;

            // Draw needs to know the total elapsed time
            // that occured for the fixed length updates.
            _gameTime.ElapsedGameTime = TimeSpan.FromTicks(TargetElapsedTime.Ticks * stepCount);
        }
    }
}
