﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Battle
{
    public class BlockDirt : Block
    {
        public BlockDirt(Map map, int x, int y) : base(map, x, y)
        {
            id = BlockID.Dirt;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Main.blockTexture["Dirt"], position, null, Color.White, 0f, origin, 1, SpriteEffects.None, 0);

            base.Draw(spriteBatch);
        }
    }
}
