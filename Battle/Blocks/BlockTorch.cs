﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace Battle
{
    public class BlockTorch : Block
    {
        public List<Block> allBlocks = new List<Block>();

        public BlockTorch(Map map, int x, int y) : base(map, x, y)
        {
            id = BlockID.Torch;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (oldId != BlockID.None)
                spriteBatch.Draw(Main.blockTexture[oldId.ToString()], position, null, Color.White, 0f, origin, 1, SpriteEffects.None, 0);
            spriteBatch.Draw(Main.blockTexture["Torch"], position, null, Color.White, 0f, origin, 1, SpriteEffects.None, 0);

            base.Draw(spriteBatch);
        }

        public void CalcLight()
        {
            lightLevel = 14;
            Queue<Block> blocks = new Queue<Block>();
            blocks.Enqueue(this);

            while (blocks.Count > 0)
            {
                Block currentBlock = blocks.Dequeue();
                int lightRef = currentBlock.lightLevel;

                for (int x = -1; x <= 1; x++)
                {
                    for (int y = -1; y <= 1; y++)
                    {
                        if (x == 0 && y == 0)
                            continue;

                        if (currentBlock.x + x < 0 || currentBlock.y + y < 0 || currentBlock.x + x >= Map.SIZE || currentBlock.y + y >= Map.SIZE)
                            continue;

                        if (x == -1 && y == -1)
                            if (map.Blocks[currentBlock.x - 1, currentBlock.y].Solid && map.Blocks[currentBlock.x, currentBlock.y - 1].Solid)
                                continue;

                        if (x == 1 && y == -1)
                            if (map.Blocks[currentBlock.x + 1, currentBlock.y].Solid && map.Blocks[currentBlock.x, currentBlock.y - 1].Solid)
                                continue;

                        if (x == -1 && y == 1)
                            if (map.Blocks[currentBlock.x - 1, currentBlock.y].Solid && map.Blocks[currentBlock.x, currentBlock.y + 1].Solid)
                                continue;

                        if (x == 1 && y == 1)
                            if (map.Blocks[currentBlock.x + 1, currentBlock.y].Solid && map.Blocks[currentBlock.x, currentBlock.y + 1].Solid)
                                continue;

                        Block b = map.Blocks[currentBlock.x + x, currentBlock.y + y];
  
                        int level = lightRef - (Math.Abs(x) + Math.Abs(y));

                        if (level <= b.lightLevel)
                            continue;

                        b.lightLevel = level;
                        allBlocks.Add(b);

                        if (b.Solid)
                            continue;

                        blocks.Enqueue(b);
                    }
                }
            }
        }

        public void UpdateNoLight()
        {
            foreach (Block b in allBlocks)
                b.lightLevel = 0;
            allBlocks.Clear();
        }

        public override void OnDestroyed()
        {
            base.OnDestroyed();

            UpdateNoLight();
            map.UpdateLight();
        }
    }
}
