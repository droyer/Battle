﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battle
{
    public class BlockNone : Block
    {
        public BlockNone(Map map, int x, int y) : base(map, x, y)
        {
            id = BlockID.None;
        }
    }
}
