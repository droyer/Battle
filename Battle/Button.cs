﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battle
{
    public class Button
    {
        int width;
        int height;
        Vector2 position;
        Vector2 origin;
        Rectangle rectangle;
        int id;
        string text;
        bool hover;
        Rectangle sourceRectangle;
        Color textColor = Color.Black;

        public Button(string text, Vector2 position, int id)
        {
            this.text = text;
            this.position = position;
            this.id = id;
            width = Main.buttonTexture.Width;
            height = Main.buttonTexture.Height / 3;
            origin = new Vector2(width / 2, height / 2);
            sourceRectangle = new Rectangle(0, height, width, height);
        }

        public void Update()
        {
            if (Main.mouseRectangle.Intersects(rectangle) && Main.active)
            {
                hover = true;
                sourceRectangle = new Rectangle(0, height * 2, width, height);
                textColor = Color.DarkRed;
            }
            else
            {
                hover = false;
                sourceRectangle = new Rectangle(0, height, width, height);
                textColor = Color.Black;
            }

            rectangle = new Rectangle((int)position.X - (int)origin.X, (int)position.Y - (int)origin.Y, width, height);
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(Main.buttonTexture, position, sourceRectangle, Color.White, 0f, origin, 1, SpriteEffects.None, 0);
            spritebatch.DrawString(Main.font, text, position, textColor, 0f,
                new Vector2(Main.font.MeasureString(text).X / 2, Main.font.LineSpacing / 2), 1, SpriteEffects.None, 0);
        }

        public bool Click(int id)
        {
            if (Main.mouse.LeftButton == ButtonState.Pressed && Main.lastMouse.LeftButton == ButtonState.Released &&
                hover && this.id == id)
                return true;
            return false;
        }
    }
}
