﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Battle
{
    public class MenuMain : Menu
    {
        public MenuMain()
        {
            AddButton("Join", Main.screenWidth / 2, Main.screenHeight / 2 - 35, 0);
            AddButton("Exit", Main.screenWidth / 2, Main.screenHeight / 2 + 35, 1);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (ButtonClick(0))
            {
                Main.menu = new MenuLoad();
                Main.Connect();
            }

            if (ButtonClick(1))
                Main.exit = true;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    spriteBatch.Draw(Main.backgroundTexture, new Vector2(x * 16, y * 16), Color.White);
                }
            }

            base.Draw(spriteBatch);
        }
    }
}
