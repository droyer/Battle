﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battle
{
    public class Menu
    {
        List<Button> buttonList = new List<Button>();
        protected int width;
        protected int height;

        public Menu()
        {
            width = (int)Math.Ceiling(Main.screenWidth / Main.scale);
            height = (int)Math.Ceiling(Main.screenHeight / Main.scale);
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (Button btn in buttonList)
                btn.Update();
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (Button btn in buttonList)
                btn.Draw(spriteBatch);
        }

        protected void AddButton(string text, float x, float y, int id)
        {
            x /= Main.scale;
            y /= Main.scale;
            buttonList.Add(new Button(text, new Vector2(x, y), id));
        }

        protected bool ButtonClick(int id)
        {
            foreach (Button button in buttonList)
            {
                if (button.Click(id))
                    return true;
            }

            return false;
        }
    }
}
